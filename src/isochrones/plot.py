import geopandas as gpd
import geoplot
import geoplot.crs as gcrs
import matplotlib.pyplot as plt

if __name__ == '__main__':
    file_name = "../../data/isochrones/BRV_45_walk.geojson"
    gdf = gpd.read_file(file_name)

    print(gdf)

    geoplot.polyplot(gdf.loc[0, :], projection=gcrs.AlbersEqualArea(),
                     edgecolor='darkgrey', facecolor='lightgrey', linewidth=.3,
                     figsize=(12, 8))

    plt.show()