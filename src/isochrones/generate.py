# -*- coding: utf-8 -*-
"""
Computes isochrones areas and associated population densities.
"""

import requests
import json
import os
import git
import geopandas as gpd
import tempfile
import pandas as pd
import concurrent.futures
from typing import List, Dict

from get_db import get_dbs, POPULATION_FILE, CONTOURS_FILE, STATIONS_DB_FILE

GIT_ROOT = git.Repo('.', search_parent_directories=True).working_tree_dir
DATA_DIR = os.path.join(GIT_ROOT, "data")
DB_DIR = os.path.join(DATA_DIR, "db")
os.makedirs(DB_DIR, exist_ok=True)
ISOCHRONES_DIR = os.path.join(DATA_DIR, "isochrones")
os.makedirs(ISOCHRONES_DIR, exist_ok=True)


def read_config_from_json() -> Dict:
    with open('config.json') as f:
        return json.load(f)


def read_stations_from_json() -> List:
    with open(STATIONS_DB_FILE) as f:
        return json.load(f)


def read_population_file() -> pd.DataFrame:
    return pd.read_excel(POPULATION_FILE, sheet_name="IRIS", skiprows=5)[["IRIS", "P20_POP"]]


def read_contours_file() -> gpd.GeoDataFrame:
    return gpd.read_file(CONTOURS_FILE)[["CODE_IRIS", "geometry"]]


def compute_time_credit_min(station: dict, root: str, isochrone_value_min: int) -> int | None:
    if station["time_from_stations_min"][root] is None:
        return None
    return isochrone_value_min - station["time_from_stations_min"][root]


def geodataframe_from_json(json_data: list | dict) -> gpd.GeoDataFrame:
    with tempfile.TemporaryFile() as fp:
        fp.write(json.dumps(json_data).encode())
        fp.seek(0)
        return gpd.read_file(fp)


def compute_isochrone(root: str, isochrone_value_min: int, profile: dict, station: dict, api_config: dict) -> gpd.GeoDataFrame | None:
    # print(f"root: {root} - isochrone: {isochrone_value_min} min - profile: {profile['name']} - station: {station['name']}")
    # print(f"time from root: {station['time_from_stations_min'][root]} min")
    time_credit_min = compute_time_credit_min(station, root,
                                              isochrone_value_min)
    # print(f"time credit: {time_credit_min} min")
    if time_credit_min is None or time_credit_min <= 0:
        # print(f"no time credit, skipping")
        return
    time_credit_min = compute_time_credit_min(
        station, root, isochrone_value_min)
    distance_credit_m = int(60. * time_credit_min * profile["speed_kmh"] / 3.6)
    params = api_config['params']
    params.update({'point': f'{station["lon_deg"]},{station["lat_deg"]}',
                   'profile': profile["api_profile"],
                   "costValue": distance_credit_m})
    response = requests.get(api_config['url'], params=params)
    return geodataframe_from_json(response.json())


def populate_isochrone(isochrone: gpd.GeoDataFrame, population_gdf: gpd.GeoDataFrame) -> gpd.GeoDataFrame:
    isochrone = isochrone.to_crs(population_gdf.crs)
    # print(isochrone)
    # print(population_gdf)
    try:
        overlay = gpd.overlay(isochrone, population_gdf,
                              how="intersection").drop(columns=["P20_POP"])
        # print(overlay)
        overlayed_iris = population_gdf[population_gdf.CODE_IRIS.isin(
            overlay.CODE_IRIS)]
        # https://gis.stackexchange.com/a/218453
        overlay = overlay.to_crs('epsg:3857')
        overlayed_iris = overlayed_iris.to_crs('epsg:3857')
        overlay["overlay_area"] = overlay["geometry"].area
        overlayed_iris["iris_area"] = overlayed_iris["geometry"].area
        overlayed_iris = overlayed_iris.drop(columns=["geometry"])
        overlay = overlay.merge(
            overlayed_iris, left_on="CODE_IRIS", right_on="CODE_IRIS")
        overlay["ratio"] = overlay["overlay_area"] / overlay["iris_area"]
        overlay["population_in_overlay"] = (
            overlay["P20_POP"] * overlay["ratio"]).astype(int)
        # print(overlay)
        isochrone["population"] = overlay["population_in_overlay"].sum()
    except TypeError as e:
        print(f"error: {e}")
        isochrone["population"] = 0
    # print(isochrone)
    return isochrone


def generate_all_isochrones(config: dict, stations: list, population_gdf: gpd.GeoDataFrame) -> None:
    final_gdf = {root: {iso: {p["name"]: {s["name"]: None
                                          for s in stations}
                              for p in config['api']['profiles']}
                        for iso in config['isochrone_values_min']}
                 for root in ["LIM", "BRV", "GRT"]}
    with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor:
        future_to_isochrone = {executor.submit(compute_isochrone,
                                               root,
                                               isochrone_value_min,
                                               profile,
                                               station,
                                               config["api"]): (root, isochrone_value_min, profile["name"], station["name"])
                               for station in stations
                               for profile in config['api']['profiles']
                               for isochrone_value_min in config['isochrone_values_min']
                               for root in ["LIM", "BRV", "GRT"]}
        for future in concurrent.futures.as_completed(future_to_isochrone):
            identifier = future_to_isochrone[future]
            try:
                gdf = future.result()
                if final_gdf[identifier[0]][identifier[1]][identifier[2]].get("all") is None:
                    final_gdf[identifier[0]][identifier[1]
                                             ][identifier[2]].update({"all": gdf})
                else:
                    res = pd.concat(
                        [final_gdf[identifier[0]][identifier[1]][identifier[2]]["all"], gdf])
                    final_gdf[identifier[0]][identifier[1]
                                             ][identifier[2]]["all"] = res
            except Exception as exc:
                print('%r generated an exception: %s' % (identifier, exc))
            else:
                print(f"{identifier} done")
    print("populating isochrones and saving to disk")
    for root_n, root_v in final_gdf.items():
        for iso_n, iso_v in root_v.items():
            for profile_n, profile_v in iso_v.items():
                if profile_v["all"] is None:
                    continue
                res = profile_v["all"].dissolve()
                isochrone = populate_isochrone(res, population_gdf)
                output_file = f'{root_n}_{iso_n}_{profile_n}.geojson'
                output_file_path = os.path.join(ISOCHRONES_DIR, output_file)
                isochrone.to_file(output_file_path, driver='GeoJSON')
                print(f"wrote {output_file_path}")


def main():
    print("starting")
    print("checking database presence, downloading if needed")
    get_dbs()
    print("done with db")
    print("read config")
    config = read_config_from_json()
    print("done with config")
    print("read stations")
    stations = read_stations_from_json()
    print("done with stations")
    print("read population database")
    population_gdf = read_contours_file().merge(read_population_file(
    ), left_on="CODE_IRIS", right_on="IRIS").drop(columns=["IRIS"])
    print("done with population database")
    generate_all_isochrones(config, stations, population_gdf)


if __name__ == '__main__':
    main()
