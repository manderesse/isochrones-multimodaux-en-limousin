# -*- coding: utf-8 -*-
"""
Fetches required population-related databases from established sources (e.g. INSEE, IGN).
"""

import os
import json
import zipfile
import sys
import shutil

import requests
import git
import gtfs_kit as gk
import geopandas as gpd
import pandas as pd
import py7zr

git_root = git.Repo(".", search_parent_directories=True).working_tree_dir
data_dir = os.path.join(git_root, "data")
db_dir = os.path.join(data_dir, "db")

# Reference targets for the population-related databases

POPULATION_FILE_URL = "https://www.insee.fr/fr/statistiques/fichier/7704076/base-ic-evol-struct-pop-2020_xlsx.zip"
POPULATION_FILENAME_ZIPPED = "base-ic-evol-struct-pop-2020_xlsx.zip"
POPULATION_FILENAME_UZ = "base-ic-evol-struct-pop-2020.xlsx"
POPULATION_FILE = os.path.join(db_dir, POPULATION_FILENAME_UZ)

CONTOURS_FILE_URL = "https://wxs.ign.fr/1yhlj2ehpqf3q6dt6a2y7b64/telechargement/inspire/CONTOURS-IRIS-PACK_2023-01$CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01/file/CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01.7z"
CONTOURS_FILENAME_ZIPPED = "CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01.7z"
CONTOURS_FILENAME_UZ = "CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01"
CONTOURS_DIR = os.path.join(db_dir, CONTOURS_FILENAME_UZ)
CONTOURS_FILE = os.path.join(
    CONTOURS_DIR,
    "CONTOURS-IRIS/1_DONNEES_LIVRAISON_2023-09-00134/CONTOURS-IRIS_3-0_SHP_LAMB93_FXX-2023/CONTOURS-IRIS.shp",
)

TIMETABLES_FILE_URL = "https://eu.ftp.opendatasoft.com/sncf/gtfs/export-ter-gtfs-last.zip"
TIMETABLES_FILENAME_ZIPPED = "export-ter-gtfs-last.zip"
TIMETABLES_DIRNAME_UZ = "export-ter-gtfs-last"
TIMETABLES_DIR = os.path.join(db_dir, TIMETABLES_DIRNAME_UZ)

ADMINEXPRESS_FILE_URL = "https://wxs.ign.fr/x02uy2aiwjo9bm8ce5plwqmr/telechargement/prepackage/ADMINEXPRESS_SHP_TERRITOIRES_PACK_2023-10-16$ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2023-10-16/file/ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2023-10-16.7z"
ADMINEXPRESS_FILENAME_ZIPPED = "ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2023-10-16.7z"
ADMINEXPRESS_FILENAME_UZ = "ADMIN-EXPRESS_3-2__SHP_LAMB93_FXX_2023-10-16"
ADMINEXPRESS_DIR = os.path.join(db_dir, ADMINEXPRESS_FILENAME_UZ)
DEPTS_FILE = os.path.join(
    ADMINEXPRESS_DIR,
    "ADMIN-EXPRESS/1_DONNEES_LIVRAISON_2023-10-16/ADE_3-2_SHP_LAMB93_FXX/DEPARTEMENT.shp",
)

STATIONS_DB_FILE = os.path.join(
    db_dir,
    "stations.json",
)

ROOT_STATIONS = {"LIM": ["87592006", "87592022"],  # Benedictins, Montjovis
                 "BRV": ["87594002"],
                 "GRT": ["87597609"]}
ROOT_STATIONS_NAMES = {"LIM": ["Limoges Bénédictins", "Limoges Monjovis"],
                       "BRV": ["Brive-la-Gaillarde"],
                       "GRT": ["Guéret"]}

# Fetch, unzip and store databases locally


def get_dbs():
    get_population_db()
    get_contour_db()
    get_adminexpress_db()
    create_stations_db()


def get_population_db():
    if not os.path.exists(POPULATION_FILE):
        print("Downloading population database...")
        response = requests.get(POPULATION_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(POPULATION_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping population database...")
        with zipfile.ZipFile(POPULATION_FILENAME_ZIPPED, "r") as zipf:
            zipf.extractall(db_dir)
        os.remove(POPULATION_FILENAME_ZIPPED)


def get_contour_db():
    if not os.path.exists(CONTOURS_FILE):
        print("Downloading contour database...")
        response = requests.get(CONTOURS_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(CONTOURS_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping contour database...")
        with py7zr.SevenZipFile(CONTOURS_FILENAME_ZIPPED, mode='r') as z:
            z.extractall(db_dir)
        os.remove(CONTOURS_FILENAME_ZIPPED)


def get_timetables_db():
    if not os.path.exists(TIMETABLES_DIR):
        print("Downloading timetables database...")
        response = requests.get(TIMETABLES_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(TIMETABLES_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping timetables database...")
        os.makedirs(TIMETABLES_DIR, exist_ok=True)
        with zipfile.ZipFile(TIMETABLES_FILENAME_ZIPPED, "r") as zipf:
            zipf.extractall(TIMETABLES_DIR)
        os.remove(TIMETABLES_FILENAME_ZIPPED)


def get_adminexpress_db():
    if not os.path.exists(ADMINEXPRESS_DIR):
        print("Downloading departments database...")
        response = requests.get(ADMINEXPRESS_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(ADMINEXPRESS_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping departments database...")
        with py7zr.SevenZipFile(ADMINEXPRESS_FILENAME_ZIPPED, mode='r') as z:
            z.extractall(db_dir)
        os.remove(ADMINEXPRESS_FILENAME_ZIPPED)


def create_stations_db():
    if os.path.exists(STATIONS_DB_FILE):
        return
    get_timetables_db()
    feed = gk.read_feed(TIMETABLES_DIR, dist_units="km")
    depts = gpd.read_file(DEPTS_FILE)
    gdf = depts[depts.INSEE_DEP.isin(["87", "23", "19"])].dissolve().to_crs('epsg:4326')
    gdf_stops = gpd.GeoDataFrame(feed.stops, geometry=gpd.points_from_xy(feed.stops.stop_lon, feed.stops.stop_lat), crs="EPSG:3857")
    j = gpd.sjoin(gdf_stops, gdf, op="within")
    t = feed.stop_times.loc[:, ['trip_id', 'arrival_time', 'departure_time', 'stop_id']]
    tt = t[t.stop_id.isin(j.stop_id)]
    tt = tt[tt.departure_time.apply(lambda x: x.split(":")[0] in list(map(lambda i: str(i), range(24))))]
    tt.loc[:, "departure_time"] = pd.to_datetime(tt.departure_time)
    tt.loc[:, "arrival_time"] = pd.to_datetime(tt.arrival_time)
    tt.loc[:, "sur_place"] = (tt.departure_time - tt.arrival_time).apply(lambda x: x.total_seconds())

    stations = {}
    for root, uic_code_list in ROOT_STATIONS.items():
        trips_to_root = tt[tt.stop_id.apply(lambda x: any(x.endswith(uic_code) for uic_code in uic_code_list))]
        stops_connecting_root = tt[tt.trip_id.isin(trips_to_root.trip_id)]
        root_durations = pd.merge(trips_to_root, stops_connecting_root,
                                  on="trip_id", suffixes=("_root", "_other"))
        root_durations = root_durations.drop(columns=["sur_place_root", "stop_id_root",
                                                      "sur_place_other"])
        root_durations_with_stop_name = pd.merge(root_durations, gdf_stops,
                                                    left_on=["stop_id_other"],
                                                    right_on=["stop_id"]).drop(columns=["stop_id_other", "geometry",
                                                                                        "location_type", "stop_url", "zone_id", "stop_desc", "stop_name", "stop_id"])
        root_durations_with_station_name = pd.merge(root_durations_with_stop_name, gdf_stops,
                                                    left_on=["parent_station"], right_on=["stop_id"]).drop(columns=["geometry", "parent_station_x", "parent_station_y", "location_type",
                                                                                                                    "stop_url", "zone_id", "stop_lon_x", "stop_lat_x",
                                                                                                                    "stop_desc", "stop_id"])
        root_durations_with_station_name.loc[:, "trip_duration"] = (
                    root_durations_with_station_name.arrival_time_other - root_durations_with_station_name.departure_time_root).apply(lambda x: x.total_seconds())
        root_durations_with_station_name = root_durations_with_station_name[root_durations_with_station_name.trip_duration > 0.]
        median_durations = root_durations_with_station_name.loc[:, ["stop_name", "trip_duration"]].groupby(["stop_name"]).median()
        root_durations_with_station_name = root_durations_with_station_name.drop_duplicates(subset="stop_name").drop(columns=["trip_duration"])
        root_durations_with_station_name = pd.merge(root_durations_with_station_name, median_durations, on="stop_name")
        root_durations_with_station_name = root_durations_with_station_name.rename(columns={"stop_name": "station_name",
                                                                                            "stop_lat_y": "lat_deg",
                                                                                            "stop_lon_y": "lon_deg",
                                                                                            "trip_duration": "time_from_root_s"})
        root_durations_with_station_name = root_durations_with_station_name.drop(columns=[c for c in root_durations_with_station_name.columns
                                                                                          if c not in ["station_name", "lat_deg", "lon_deg", "time_from_root_s"]])
        for station in root_durations_with_station_name.to_dict("records"):
            time_from_root_s = station.pop("time_from_root_s")
            if station["station_name"] not in stations:
                station.update({"time_from_stations_min": {root: int(time_from_root_s/60.)}})
                stations.update({station["station_name"]: station})
            else:
                old_station = stations.pop(station["station_name"])
                old_station["time_from_stations_min"].update({root: int(time_from_root_s/60.)})
                stations.update({station["station_name"]: old_station})

    for root in ROOT_STATIONS.keys():
        for _, station in stations.items():
            if root not in station["time_from_stations_min"].keys():
                station["time_from_stations_min"].update({root: None})
            if station["station_name"] in ROOT_STATIONS_NAMES.get(root):
                station["time_from_stations_min"].update({root: 0})

    stations_final = []
    for station in stations.values():
        station_name = station.pop("station_name")
        station.update({"name": station_name})
        station = {k: station[k] for k in ["name", "lat_deg", "lon_deg", "time_from_stations_min"]}
        stations_final.append(station)

    stations_final = sorted(list(stations_final), key=lambda s: s["name"])
    with open(STATIONS_DB_FILE, "w") as f:
        json.dump(stations_final, f, indent=4)
    shutil.rmtree(TIMETABLES_DIR, ignore_errors=True)



def main():
    get_dbs()


if __name__ == "__main__":
    main()
