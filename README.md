# Isochrones multimodaux en Limousin

## TODO

- TODO : Use self-hosted OTP service for isochrone computations
- TODO : Generate nicer maps using QGis

- DONE : combine polygons for a given city+time+profile into a single polygon
- DONE : intersect the polygon with the population grid to estimate the population in the polygon
  - DONE : get the IRIS grid - source https://geoservices.ign.fr/contoursiris
  - DONE : get the population of each IRIS and add it to the grid (geopandas join) - source recensement 2019 https://www.insee.fr/fr/statistiques/6543200
  - DONE : intersect each polygon with the IRIS grid, compute the surface ratio on each polygon, and multiply the population of each IRIS by this ratio
- DONE : plot the polygon on a map with contextual information (roads, cities, etc.)
- DONE : Automate the generation of a stations database, using the following resources:
  - timetables : https://transport.data.gouv.fr/datasets/horaires-des-lignes-ter-sncf
  - stations geoloc : https://ressources.data.sncf.com/explore/dataset/liste-des-gares/information/
  - Get Region / Departement boundaries from https://geoservices.ign.fr/adminexpress
- DONE : Call DB generation functions from the main script if DB is missing locally

## Installation

First, get pipenv : https://pipenv-fork.readthedocs.io/en/latest/index.html#install-pipenv-today

At the root of the project, run:
```
pipenv install
```

## Usage

### Generate isochrones

Go to the isochrone folder:

```
cd src/isochrones
```

Activate the pipenv shell:

```
pipenv shell
```

Run the script:

```
python generate.py
```

Data will be downloaded to the `data` folder at the root of the repository.

Configuration options are available in the `config.json` file.

### Visualize an isochrone

Go to the isochrone folder:

```
cd src/isochrones
```

Activate the pipenv shell:

```
pipenv shell
```

Run the script:

```
python plot.py
```

Edit the `plot.py` file to change the isochrone to plot.